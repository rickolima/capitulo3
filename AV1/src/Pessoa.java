public class Pessoa implements Comparable<Pessoa>
{
    private int id;
    private String nome;
    private int idade;
    
    public Pessoa(final int id, final String nome, final int idade) {
        this.id = id;
        this.nome = nome;
        this.idade = idade;
    }
    
    public void cadastra(final int id, final String nome, final int idade) {
        this.id = id;
        this.nome = nome;
        this.idade = idade;
    }
    
    public int getId() {
        return this.id;
    }
    
    public String getNome() {
        return this.nome;
    }
    
    public int getIdade() {
        return this.idade;
    }
    
    @Override
    public int compareTo(final Pessoa o) {
        if (this.idade < o.getIdade()) {
            return -1;
        }
        if (this.idade > o.getIdade()) {
            return 1;
        }
        return 0;
    }
}
