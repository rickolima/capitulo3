
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;

public class CachePessoa
{
    static List<Pessoa> lista;
    
    static {
        CachePessoa.lista = new ArrayList<Pessoa>();
    }
    
    public void addPessoa(final int id, final String nome, final int idade) {
        CachePessoa.lista.add(new Pessoa(id, nome, idade));
    }
    
    public Pessoa getPessoa(final int id) {
        for (final Pessoa item : CachePessoa.lista) {
            if (item.getId() == id) {
                return item;
            }
        }
        return null;
    }
}