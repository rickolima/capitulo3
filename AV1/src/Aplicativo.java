import java.util.Collections;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.List;

public class Aplicativo
{
    static int index;
    static List<Pessoa> lista;
    static CachePessoa cache;
    static Scanner tecla;
    
    static {
        Aplicativo.index = 0;
        Aplicativo.lista = new ArrayList<Pessoa>();
        Aplicativo.cache = new CachePessoa();
        Aplicativo.tecla = new Scanner(System.in);
    }
    
    public static void main(final String[] args) {
        int op;
        do {
            System.out.println("=================================================");
            System.out.println("||------------------MENU-----------------------||");
            System.out.println("=================================================");
            System.out.println("||-Op\u00e7\u00f5es-||-----Resumo------------------------||");
            System.out.println("=================================================");
            System.out.println("||---1----||-Cadastrar Pessoas-----------------||");
            System.out.println("||---2----||-Ordenar por idade e listar pessoas||");
            System.out.println("||---3----||-Consultar pessoa------------------||");
            System.out.println("=================================================");
            System.out.println("||---4----||-Sair------------------------------||");
            System.out.println("=================================================");
            System.out.println("||-Digite sua op\u00e7\u00e3o: ");
            op = Integer.parseInt(Aplicativo.tecla.nextLine());
            switch (op) {
                default: {
                    continue;
                }
                case 1: {
                    cadastrarPessoas();
                    continue;
                }
                case 2: {
                    ordenarPorIdadeListarPessoas();
                    continue;
                }
                case 3: {
                    consultarPessoa();
                    continue;
                }
            }
        } while (op != 4);
    }
    
    private static void consultarPessoa() {
        System.out.println("=================================================");
        System.out.println("||---Lista de pessoas ordenadas por idade:-----||");
        System.out.println("=================================================");
        System.out.println("|| Digite o id da pessoa que deseja consultar: ||");
        final int id = Integer.parseInt(Aplicativo.tecla.nextLine());
        System.out.println("=================================================");
        System.out.println("||-----ID-----||---Idade---||------Nome--------||");
        System.out.println("=================================================");
        Pessoa itemp = null;
        if (Aplicativo.cache.getPessoa(id) != null) {
            itemp = Aplicativo.cache.getPessoa(id);
        }
        else {
            for (final Pessoa item : Aplicativo.lista) {
                if (item.getId() == id) {
                    itemp = item;
                    Aplicativo.cache.addPessoa(item.getId(), item.getNome(), item.getIdade());
                }
            }
        }
        if (itemp != null) {
            System.out.println("||-----" + colocarZero(itemp.getId()) + "-----||-----" + colocarZero(itemp.getIdade()) + "----||-----" + itemp.getNome() + "-------||");
        }
        else {
            System.out.println("||---Pessoa n�o cadastrada!--------------------||");
        }
        System.out.println("=================================================");
    }
    
    private static void ordenarPorIdadeListarPessoas() {
        Collections.sort(Aplicativo.lista);
        listarPessoas();
    }
    
    private static void listarPessoas() {
        System.out.println("=================================================");
        System.out.println("||---Lista de pessoas ordenadas por idade:-----||");
        System.out.println("=================================================");
        System.out.println("||---Idade---||-----ID-----||------Nome--------||");
        System.out.println("=================================================");
        int total = 0;
        for (final Pessoa item : Aplicativo.lista) {
            System.out.println("||-----" + colocarZero(item.getIdade()) + "----||-----" + colocarZero(item.getId()) + "-----||-----" + item.getNome() + "-------||");
            ++total;
        }
        System.out.println("=================================================");
        System.out.println("||Total de pessoas cadastradas:.............." + colocarZero(total) + "||");
        System.out.println("=================================================");
    }
    
    private static String colocarZero(final int id) {
        if (id < 10) {
            return "0" + id;
        }
        return new StringBuilder().append(id).toString();
    }
    
    private static void cadastrarPessoas() {
        ++Aplicativo.index;
        System.out.println("=================================================");
        System.out.println("||---Lista de pessoas ordenadas por idade:-----||");
        System.out.println("=================================================");
        System.out.println("|| id =>" + colocarZero(Aplicativo.index) + "                                     ||");
        System.out.println("|| Digite o nome da pessoa:                    ||");
        final String nome = Aplicativo.tecla.nextLine();
        System.out.println("|| Digite a idade da pessoa:                   ||");
        final int idade = Integer.parseInt(Aplicativo.tecla.nextLine());
        Aplicativo.lista.add(new Pessoa(Aplicativo.index, nome, idade));
        System.out.println("=================================================");
        System.out.println("||---Pessoa cadastrada com sucesso!------------||");
        System.out.println("=================================================");
    }
}