
import java.util.Scanner;
import javax.swing.JOptionPane;

public class Exercicio4 {

	//Declara��o de variaveis globais
	static Scanner tecla = new Scanner(System.in);
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	//Declara��o de variaveis locais e constantes
	double pot_lamp, larg_com, comp_com, area_com, pot_total, result;
	double num_lamp;
	
		
		
	//Entrada de dados
	System.out.println("Qual a pot�ncia da l�mpada em watts?");
	pot_lamp = tecla.nextDouble();
	System.out.println("Qual a largura do c�modo em metros?");
	larg_com = tecla.nextDouble();
	System.out.println("Qual o comprimento do c�modo em metros");
	comp_com = tecla.nextDouble();
		
		
	//Processamento de dados
	area_com = larg_com * comp_com;
	pot_total = area_com * 18;
	
		
	num_lamp =  pot_total/pot_lamp;
	
	
	
	//Saida de dados
	System.out.println("Numero de lampadas necess�rias para iluminar esse c�modo: " + (int)num_lamp);	
		
		
		
		
		
		
		
	}

}